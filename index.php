<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Intro | Activity 1</title>
</head>
<body>
    <h1>Activity 1</h1>
    
    <?php
        function getFullAddress($address, $city, $province, $country){
            echo "$address, $city, $province, $country";
        }

        echo("<h2>Full Address</h2>");
        getFullAddress("3F Caswynn Bldg., Timog Avenue", "Quezon City", "Metro Manila","Philippines");
        echo("<br/>");
        echo("<br/>");
        getFullAddress("3F Enzo Bldg., Buendia Avenue", "Makati City", "Metro Manila","Philippines");
    ?>

</body>
</html>